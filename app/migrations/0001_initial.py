# Generated by Django 3.1.7 on 2021-03-24 19:13

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
                ('imagen', models.ImageField(blank=True, upload_to='productos')),
                ('marca', models.CharField(max_length=200)),
                ('calorias', models.CharField(max_length=200)),
                ('hidratos', models.CharField(max_length=200)),
                ('grasa', models.CharField(max_length=200)),
                ('proteinas', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
            ],
        ),
    ]
